# quest

A library for Dart developers. It is awesome.
Collecting some #Dartlang code snippets

## Usage

A simple usage example:

    import 'package:quest/quest.dart';

    main() {
      var awesome = new Awesome();
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
