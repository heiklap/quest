/// hkl  Started:   4.11.2016  sudoku_solver  loc:  1300 > 1699 > 1420
/// Variables:   103  lol
///   version  0.0.1  ///  In glorious DartLang web programming language
/// Description: Messy walk to unknown, without prior design or planning.
///
/// USAGE:  Copy this script and run it in:  https://dartpad.dartlang.org
/// Output: console.        In :browser page: not yet
/// readyState: 83  %   Success: YES   Solved: 81     :runTime: 780 ms
///
///  principle:  when #pointer comes to 0 node, program counts *every*time*
///              what numbers are possible in that node
///              depending on row, col, and cube ( = 3x3 box)3333
/// #cubix is 3x3,  #block is 3 x 9 horizontal area
///    solved ALL in medium, and in Difficult(with little help)
///
///         **  About statistics and used words  **
/// :Words: :NAME 3  :Messy 7  :FUNNY 3   :GLORY 2  :lol 8 :Used 7
/// :Word 6    :DN 0 = DevNote, :LEARN 5    :LANG: 3    :QUEST (c 11)
///  :WOP: WorkOnProcess
/// :Words :Statistics: :UGLY 0   round= 81 times
/// :word  Count  90  times    consider avoiding more
/// hklTry ( c 8)  =  me trying something.. special
///        **  TO  DO  45  pieces, :)   **
// TODO  :word  cube  is  8 times in code and comments
// TODO  change ??  it to::   cubix, 62 !! uses >> easy find and consistency
// TODO  Learn:  successC == 0 ? failedC++ : '''

/// :QUEST  Error in verticalCheck   makes many  wrong numbers
///                  **  next planning  **
/// NEXT Print output is too long ( in vertical)
/// NEXT Save this to DartPad :)  and THEN:   Solve BUG in verticalCheck
/// NEXT Make DN:  as: devNote, like: DN:WORD:   DN:LANG
// LANG: Use set; ['unknown', 'blockSolver', 'verticalCheck', 'solveSudoku']
///DONE                  **      just done    **
/// The Crowd is yelling to-get new properties..33
/// Three getXX methods in hh-class to mark nodes to #base
///                  **      ideas, code  aso.      **
///howTo  add map:  .putIfAbsent(6, () => 5);
///howTo print a map  .forEach((k,v)=>print("out: $k $v"));
/// "It's important to realize that final affects the variable, but not the
/// object pointed to by the variable. / source: stackOverflow
/// That is, final doesn't make the variable's object immutable."
///                  ***   note to users    **
///  If you have installed Dart SDK, you can use command line tools
///  >>  to use Dart Observatory cl tool:   dart --observe <script>.dart
///  Open a browser to http://localhost:8181 to see the Observatory UI.
///  Watch:  FORK (and COPY) aso. commands in bitbucket
///
/*
-- 99 SolvedHereNow by Heikki
Sudoku Difficult: With checkRo
RowCheck:   2 1 3 4 9 5 6 8 7
RowCheck:   4 8 5 7 6 2 1 3 0
RowCheck:   9 7 6 1 3 8 2 5 4
RowCheck:   5 6 4 9 8 3 0 1 2
RowCheck:   1 2 7 5 0 6 3 4 9
RowCheck:   8 3 9 0 2 4 0 7 6
RowCheck:   6 4 1 3 5 9 7 2 8
RowCheck:   7 5 2 8 0 1 4 9 3
RowCheck:   3 9 8 2 4 7 5 6 1
 */

import 'dart:math'; // using random generator to get x-row / x-col

String actor = 'main'; //  mark blocks that can update matrix99

///  keeping this stuff temporary in upper level
void spiderInfo() {
  print('\n -------------- spider Info --------------------');
  String spiderCountCalledS = rr.spiderCountCalled.toString();
  String spiderCountRoundS = rr.spiderCountRound.toString();
  String spiderCountTestedS = rr.spiderCountTested.toString();
  String spiderCountSolvedS = rr.spiderCountSolved.toString();

  print('spiderCountCalled:     $spiderCountCalledS ');
  print('spiderCountRound:      $spiderCountRoundS ');
  print('spiderCountTested:     $spiderCountTestedS');
  print('spiderCountSolved:     $spiderCountSolvedS');
  print('-------------- spider Info done ---------------- \n');
}

// Some StringBuffers >> output, not used: yet
StringBuffer outputBuf = new StringBuffer();
StringBuffer errorBuf = new StringBuffer();
StringBuffer foundBuf = new StringBuffer();
int valueOnlyPossibleHereC = 0; //  6

///  Class to collect upper-level Sudoku variables
class SudokuSolver {
  bool solvedB = false; // 7
  bool failedB = false; // 11 / 5
  // QUEST:  hotWo make this, and some other Lists, a fixed length list?
  // List<int> posValuesL = new List(9);
  //  final List<int> posValuesL = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  final List<int> posValuesL = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  final int sudokuGoal = 81;
  final int rowGoal = 45;
  final List<int> threeNodeBaseL = [0, 3, 6];

  //  Starting to move outPut to StringBuffers
  StringBuffer ssBuf = new StringBuffer();

  ///  This will be the ONLY place WHERE ALL ACTUAL node fill is happening
  updateNode(int updX, int updY, int _nbrToUpdate, String actor) {
    infoNumberFound(_nbrToUpdate);
    //  Yep update BUT WITH NEW NUMBER, not _nbr
    matrix99[updX][updY] = _nbrToUpdate;
    cc.solvedLCount++; //  11 uses
    //  Add solved to lists...(?)
  }

//  call:  (x , y,_guL  by matrixCheckNode  loc:  60
  bool updateOneOfTwo(int _Cx, int _Cy, List<int> _twoValuesL) {
    //  print('-------- updateOneOfTwo ----------------------------');
    bool retBool = false;

    List<int> _cubixBaseL = [];
    _cubixBaseL.addAll(cubixFindBaseLF(_Cx, _Cy));
    int _cBx, _cBy;
    _cBx = _cubixBaseL[0];
    _cBy = _cubixBaseL[1];

    List<int> _cubixDataL = [];
    for (var _Xx = _cBx; _Xx < _cBx + 3; _Xx++) {
      for (var _Yy = _cBy; _Yy < _cBy + 3; _Yy++) {
        if (matrix99[_Xx].elementAt(_Yy) > 0) {
          _cubixDataL.add(matrix99[_Xx].elementAt(_Yy));
          //  parameter _twoValuesL is here to use
          //  in node _Cx, _Cy
//TODO  code:
          //  Easy code to-get update.
          //  use updateNode()

        }
      } // -- col
    } // -- row

    //  print('-------- updateOneOfTwo done------------------------ \n');
    return retBool;
  }

  ///  function to check:  update matrix99[row][col]  with new NODE _nbr
  ///  This should be the ONLY place, where update happens
  bool matrixCheckNode(int x, int y, int _nbr, String actor) {
    bool retBool = false;
    int _nbrToUpdate; // lol  do not use _nbr

    if (matrix99[x][y] == 0) {
      List<int> _guL = [];
      _guL.addAll(valuesPossibleHereLF(x, y, _nbr));

      if (_guL.length == 1) {
        _nbrToUpdate = _guL[0];
        String _guLS = _guL.toString();
        String _guLLengthS = _guL.length.toString();
        String _vS = _guL[0].toString();
        print(':MCN:FoundOnlyOneValueP L: $_guLS L: $_guLLengthS V: $_vS  \n');

        updateNode(x, y, _nbrToUpdate, actor);

        String answerRowS = matrix99Answer[x].toString();
        String sLCountS = cc.solvedLCount.toString();
        cc.solvedL.add(
            ' $sLCountS ;MCN:SolvedAT: $x $y num:   $_nbrToUpdate  BY $actor  Ans: $answerRowS');
        cc.solvedHandlerCount++;
        retBool = true;
      }
      if (_guL.length == 2) {
        //  Coing to solve hard cases
        retBool = (updateOneOfTwo(x, y, _guL));
      }
      actor = 'unkonown after MCN';
    }
    return retBool;
  } //  ----------  MCN

  ///  Shouting: HOORAY!!  We solved one number
  void infoNumberFound(int _foundI) {
    /// print('Found-Number:  Actor: $actor     $_foundI  XX \n');
    /// Code: here, if needed
  }

  /// check Row vor existence of val
  /// Name here all usual three parameters, even if only 2 are used
  bool valueXIsInRow(int r, int c, int v) {
    bool retBool = false;
    if (matrix99[r].indexOf(v) > -1) retBool = true;
    return retBool;
  }

  /// check column: >  variables existence
  bool valueXIsInCol(int r, int c, int v) {
    bool retBool = false;
    List<int> _l = [];
    _l.addAll(matrixColumnFL(c));
    if (_l.indexOf(v) > -1) retBool = true;
    //  matrix99[];
    return retBool;
  }

  /// check cubix
  /// This is called by row and column.  Not baseColumn and..
  /// cubix methods finds those base values
  bool valueXIsInCubix(int r, int c, int v) {
    bool retBool = false;
    List<int> _l = [];
    _l.addAll(cubixGetData(r, c));
    if (_l.indexOf(v) > -1) retBool = true;
    //  matrix99[];
    return retBool;
  }

  /// check cubix, rowHandler and spider used:
  bool valueXCanBeInNode(int r, int c, int v) {
    bool retBool = true;
    if (valueXIsInRow(r, c, v)) retBool = false;
    if (valueXIsInCol(r, c, v)) retBool = false;
    if (valueXIsInCubix(r, c, v)) retBool = false;
    //  matrix99[];
    return retBool;
  }

  /// Returns true, if value is possible in this node
  /// This is newest of this kind of function here. Is this elegant?
  ///  Used:  ONLY ONCE:  by :MCN:
  List<int> valuesPossibleHereLF(int r, int c, int _v) {
    valueOnlyPossibleHereC++;

    foundBuf.writeln('--- valuesPossibleHereLF data  ---');
    // collecting data to foundBuf StringBuffer
    foundBuf.writeln(posValuesL.length);
    foundBuf.writeln(posValuesL);
    foundBuf.writeln('--- was posValuesL  ------------');
    foundBuf.writeln('');
    foundBuf.writeln(' $r  $c   $_v');
    //  getting all 9 possible values to List
    List<int> _pvL = [];
    _pvL.addAll(ss.posValuesL);
    // print(matrix99[r]);
    // print(_pvL);
    // catching #NotPossible values
    List<int> _notPvL = [];
    List<int> _colList = matrixColumnFL(c);
    foundBuf.writeln(_colList);
    foundBuf.writeln(_colList.length);
    foundBuf.writeln('-------------  was col list and length');

    List _gCD = cubixGetData(r, c);
    foundBuf.writeln(_gCD);
    foundBuf.writeln(_gCD.length);
    foundBuf.writeln('----------------  was  gCD  ------------');

    _notPvL.addAll((matrix99[r]).where((x) => x > 0));
    foundBuf.writeln(_notPvL);
    foundBuf.writeln(_notPvL.length);
    foundBuf.writeln('----------------  was  _notPvL  ------------');
    _notPvL.addAll((_colList).where((x) => x > 0));
    foundBuf.writeln(_notPvL);
    foundBuf.writeln(_notPvL.length);
    foundBuf.writeln('----------------  was  _notPvL  ------------');
    _notPvL.addAll((_gCD).where((x) => x > 0));
    foundBuf.writeln(_notPvL);
    foundBuf.writeln(_notPvL.length);
    foundBuf.writeln('----------------  was  _notPvL  ------------');

    _notPvL.removeWhere((x) => x == 0); //  should not be here
    foundBuf.writeln(_notPvL);
    foundBuf.writeln(_notPvL.length);
    foundBuf.writeln('----------------  was LAST   _notPvL  ------------');

    foundBuf.writeln(_notPvL.length);
    foundBuf.writeln(_pvL.length);
    foundBuf.writeln(_pvL);
    foundBuf.writeln('------------  ready to change _pvL    ------------');

    // TODO  add cubixCheck
    _pvL.removeWhere((x) => _notPvL.indexOf(x) > -1);
    foundBuf.writeln(_pvL.length);
    foundBuf.writeln(_pvL);
    foundBuf.writeln('------------  changed _pvL    ------------');

    foundBuf.writeln('::valuesPossibleHereLF::: $r  $c   $_pvL');

    if (_pvL.length == 1) {
      String _vbOnlyS = _pvL[0].toString();
      //  print(':vopH:  _pvL-lngth = 1  vbOnly: $_vbOnlyS ');
      //  print('::valuesPossibleHere LF R,C,V,actor:  $r  $c   V: $_vbOnlyS  $actor ');
      foundBuf.writeln(
          '::valuesPossibleHereLF  ValueFound:  $r  $c   V: $_vbOnlyS  $actor ');
    }

    foundBuf.writeln('------ one  valuesPossibleHereLF  --------------- \n');
    foundBuf.writeln('--- valuesPossibleHereLF data done  ---');
    return _pvL;
  }

//  Same asabove, but with no debug and prints
  List<int> vbHereLF(int r, int c, int _v) {
    valueOnlyPossibleHereC++;
    //  getting all 9 possible values to List
    List<int> _pvL = [];
    _pvL.addAll(ss.posValuesL);
    // catching #NotPossible values
    List<int> _notPvL = [];
    List<int> _colList = matrixColumnFL(c);

    List _gCD = cubixGetData(r, c);

    _notPvL.addAll((matrix99[r]).where((x) => x > 0));
    _notPvL.addAll((_colList).where((x) => x > 0));
    _notPvL.addAll((_gCD).where((x) => x > 0));

    _notPvL.removeWhere((x) => x == 0); //  should not be here

    // TODO  add cubixCheck

    _pvL.removeWhere((x) => _notPvL.indexOf(x) > -1);

    if (_pvL.length == 1) {
      print('::vopHereLF  ValueFound:  $r  $c   V: $_v  $actor ');
    }
    return _pvL;
  } //  -----  vbHereLF

  void reportSudoku() {
    //  used: 1
    print(':END:info  = = = = = = = =  ss-report  = = == = = = =  \n');
    matrixShow(matrix99, 'Sudoku Report');
    //  getting data from class to temporary variables >>  printing
    String posValuesLLS = posValuesL.length.toString();
    print('Possible values:   $posValuesLLS   $posValuesL ');

    String failedBS = failedB.toString();
    String solvedBS = solvedB.toString();
    print('Failed? :  $failedBS      Solved:  $solvedBS');
    print('= = = = =  = = = = = = = =  ss-report done  = = == = = = =  \n');
  }
} //  -----  class SudokuSolver

//  Creating instance of class with classes default constructor
var ss = new SudokuSolver();

///  implementing counting and control in loop ( solveSudoku() )
class RoundRunner {
  String name = "Sudoku Round Runner";
  //  limit rounds in development phase to avoid eternal looping
  final int roundMax = 12; //  escape from eternal loop
  int roundCount = 0; // set to 0 meanwhile, so not total rounds
  bool firstRoundB = true;
  bool lastRoundB = false;
  int successC = 0;
  int failedC = 0;
  int successTotalC = 0; //  not used:
  int failedTotalC = 0; //  not used:
  int failedRoundLimit = 10;
  int roundCountTotal = 0; //  not used:

  int solvedInRowHandler = 0;
  int blockSolverCount = 0;
  int spiderCountCalled = 0; // new
  int spiderCountRound = 0; // new
  int spiderCountTested = 0; // new

  int spiderCountSolved = 0; // new

  void info() {
    String ssRoundCountS = roundCount.toString();
    print(
        '--------------- round Runner: $ssRoundCountS ----------------------');
    String successCS = successC.toString();
    String failedCS = failedC.toString();
    print('SolvedInSSRound: $successCS   FailedRoundsInSS:  $failedCS  ');
    print('--------------- round Runner  done ----------------------');
  }

  String infoEnd() {
    String retS = successC.toString();
    return retS;
  }

  void reportRunner() {
    print('----------------  runner-report -------------------------');
    String roundCountS = rr.roundCount.toString();
    String rrRoundMAxS = rr.roundMax.toString();
    print(
        ' rr.roundMax: $rrRoundMAxS  (use 100-400 ??) to escape eternal loop');
    print('roundCount  $roundCountS');

    print('----------------  runner-report done ---------------------');
  }
}

var rr = new RoundRunner();

/// class to separate statistics from solver
/// cc. used:  25 times
class CounterClown {
  ///  variables to-get statistics
  List<String> solvedL = []; //  10
  int solvedLCount = 0;

  Map<int, int> roundM = new Map();
  Map<int, int> solvedM = new Map();
  Map<int, int> solvedInBlockSolverM = new Map();

  //  :word : solved  83  times
  int nodeHandlerCount = 0; // every > 0 node touched
  int solvedHandlerCount = 0; // 2 uses  CHECK
  //  also local _xxx

  int solvedAtBeginning = 0;
  int solvedAtEnd = 0;
  int unSolvedAtEnd = 0;

  //  Starting to use StringBuffer >> output
  StringBuffer ccBuf = new StringBuffer();
  void infoCounterMap() {
    //  map1.forEach((k,v)=>print("out: $k $v"));
    print('------------------ cc solved  Map  --------------');
    solvedM.forEach((k, v) => print('$k $v'));
    print('---------------------------------------------------');
  }
}

//  instance of counter Class.  get properties by  cc.
var cc = new CounterClown();

//  HelperHero class >> simple, common matrix handle methods
//  hh. used:  only  3 times
class HelperHero {
  String name = 'Helper Hero';

  /// dropping values [2, 5,  7] to base values 0,  3,  6
  /// :Kudos to Günter Zöchbauer in Slack about help
  int getNearestLower(int target, List<int> baseList) {
    final lowers = baseList.where((val) => val <= target);
    return lowers.last;
  }

  ///  returning  0, 3, 6   from source  2, 5, 8
  List<int> getBaseNodes(List<int> list, List<int> baseList) {
    List<int> retL = [];

    /// comment:
    retL.addAll(list.map((val) => getNearestLower(val, baseList)).toList());
    return retL;
  }

  ///  getting  3  if  that is missing from  values
  List<int> getMissingBase(List<int> list, List<int> baseList) {
    List<int> retL = [];
    List<int> resultList = [];
    resultList.addAll(getBaseNodes(list, baseList));
    for (var z in baseList) {
      if (resultList.indexOf(z) == -1) retL.add(z);
    }
    return retL;
  }

  /// TO-DO ?    meant to find two ZERO-value positions in XXX  row
  List<int> twoZeroInThree(int _startPos) {
    //TODO  used:  0 !!
    List<int> _retList = []; //  to hold two integers
    return _retList; //  like  [3,5]
  }

  ///  check if two 0's exist in  xxx  yyy   zzz  of List<int>
  ///  :MESSY  Example of how you have time to write, but not time to think. :lol
  int twoZerosColumn(int _row) {
    // not :used. ? Is this useful?
    //TODO  used: 0 !!
    int _baseColumnI = -1;
    //  bool _retB = false;
    List<int> checker(List<int> _l) {
      List<int> _retList = _l;
      _retList.removeWhere((e) => e > 0);
      return _retList;
    }

    // if (checker(matrix99[_row].sublist(0,2))) = [0,0]){
    if (checker(matrix99[_row].sublist(0, 2)) == [0, 0]) {
      _baseColumnI = 0;
    }
    if (checker(matrix99[_row].sublist(3, 5)) == [0, 0]) {
      _baseColumnI = 3;
    }
    if (checker(matrix99[_row].sublist(6, 8)) == [0, 0]) {
      _baseColumnI = 6;
    }
    String _rowS = _row.toString();

    String _baseColumnIS = _baseColumnI.toString();
    print('::TWO-ZERO-RETURN: Row: $_rowS   Num: $_baseColumnIS ');
    return _baseColumnI;
  }

  ///  list to find node to-get mum  missing in xxx  area
  ///  Glorious and very understandable name
  List twoNodeGetMissing(first, second) {
    List retList = new List();
    //:QUEST  You can do this better
    List<int> l = [];
    l.addAll(ss.threeNodeBaseL); //  = [0, 3, 6];
    if (first < 3) l.remove(0);

    if (first > 5) l.remove(6);
    if ((first > 2) && (first < 6)) l.remove(3);

    if (second < 3) l.remove(0);

    if (second > 5) l.remove(6);

    if ((second > 2) && (second < 6)) l.remove(3);

    retList.addAll(l);

    return retList;
  }

  /// Just an empty idea,  not used:
  int oneOfThreeColumns(int first, int second) {
    // choose one of three vertical columns
    int _retCol;

    return _retCol;
  }
} //  ------------------------   class HelperHero

var hh = new HelperHero();

///  Tests and such functions. tc. used: 6
class TestClass {
  //  Starting maybe soon to use StringBuffer for output
  StringBuffer tcBuf = new StringBuffer();

  /// using random generator to-get... something
  /// IDEA:
  void testRandom() {
    //  code:
    var random = new Random(279);
  }

  ///  testing random aso.  little :MESSY
  void testProperties() {
    print(
        '\n --------------- testing sudoku properties -----------------------------');
    print(' test matrix rows:::');

    print('matrix  0');

    print(' test matrix RANDOM  rows:::');
    var randomGen = new Random(8);
    int nRandom = randomGen.nextInt(8);
    print('random is: $nRandom:');
    print(matrix99[nRandom]);

    int oRandom = randomGen.nextInt(8);
    print('$oRandom  and one more randomGen8 :');
    print(matrix99[oRandom]);
    print(matrix99[randomGen.nextInt(8)]);

    print('matrix Row 8:');
    print(matrix99[8]);
    matrixShow(matrix99, ' Matrix 99');
    print(
        '------------------ testing sudoku properties done ---------------------- \n');
  }

  ///  3 x 3 area testing
  void testCubix(String msg) {
    print(':TCubix: -------- finding cubix base## $msg  -------------------');
    print(cubixFindBaseLF(2, 6)); //  [0, 6] DATA: WRONG  [6, 3] RIGHT: 137
    print(cubixFindBaseLF(4, 8)); //  [3, 6] DATA:  [6, 3]
    print(cubixFindBaseLF(5, 2)); //  [3, 0] DATA:OK [8, 7]
    print(cubixFindBaseLF(6, 3)); //  [6, 3] DATA:OK [9, 7, 3, 8]

    print(':TCubix:-------- getting cubix data ## $msg -------------------');
    print('  Function CubixValuesLF- x-y-     returns _cubeL-9xN > 0');
    print('1.  Function cubixGetData  >>  2  .. cubixFindBaseLF ');
    print('..  and calls  CubixValuesLF ');

    print(cubixGetData(0, 1)); // [7, 8, 5]  OK  all
    print(cubixGetData(1, 5)); // [1, 9, 4, 3]
    print(cubixGetData(2, 7)); // [1, 3, 7]

    print(cubixGetData(3, 0)); // [8, 7]
    print(cubixGetData(4, 3)); // [3, 2, 5]
    print(cubixGetData(5, 8)); // [6, 3]

    print(cubixGetData(7, 2)); // [4, 1, 6]
    print(cubixGetData(8, 4)); // [9, 7, 3, 8]
    print(cubixGetData(6, 6)); // [1, 8, 2]
    print(
        ':TCubix:-------- getting cubix ## $msg data done-------------------\n');
  }

  ///  hh-class giving base nodes depending on lists
  void testBaseNodes() {
    List<int> ll = [];
    print('------testBaseNodes  getMissingBase ------------------');
    ll.addAll(hh.getMissingBase([2, 4], ss.threeNodeBaseL));
    print(ll);
    ll.clear(); //  waiting 6 OK

    ll.addAll(hh.getMissingBase([5, 7], ss.threeNodeBaseL));
    print(ll); //  waiting  0
    ll.clear();
    ll.addAll(hh.getMissingBase([1, 8], ss.threeNodeBaseL));
    print(ll); //  waiting 3
    ll.clear();
    print('------testBaseNodes   getMissingBase done ------------------');
  }
} //  -------------   TestClass

var tc = new TestClass();

/// howT0 name this class?. chk.  used::  11
class CheckerClass {
  //  Starting to use StringBuffer to-save output
  //  Huh, these names little hurt
  StringBuffer chkBuf = new StringBuffer(); // not used

//  how many NODE solved at the beginning
// :QUEST  can you make this better ?? No. YES!!
// :NAME   stop using "begin"  in property names
  void beginStats(bool _show) {
    //  usage: 1
    actor = "Begin";
    for (var c = 0; c < 9; c++) {
      var l = matrixColumnFL(c); // can this be better?
      for (var z in l) {
        if (z > 0) {
          cc.solvedAtBeginning++;
          cc.solvedLCount++;
          String solvedLCountS = cc.solvedLCount.toString();
          cc.solvedL.add('$solvedLCountS SolvedAT: $c $z num: $l BY $actor');
        }
      }
    }
    if (_show) {
      print(':begStat: -------------------------');
      String ccSolvedAtBeginningS = cc.solvedAtBeginning.toString();
      print('         cc.solvedAtBeginning   $ccSolvedAtBeginningS');
      String ccSolvedLCountS = cc.solvedLCount.toString();
      print('         cc.solvedLCount  $ccSolvedLCountS');
      print(':begStat: ------------------------- \n');
    }
    actor = "unknown after begin Stats";
  } //     -----     beginStats

  ///  what number is missing in READY sudoku row??
  List<int> checkMissingInRow(int row) {
    //  usage: 1  below
    print(matrix99[row]);
    List<int> mirL = [];
    List<int> pvL = [];
    //TODO  Fancy testing >> List<int>
    pvL.add(1);
    pvL.add(2);
    pvL.add(3);
    pvL.add(4);
    pvL.add(5);
    pvL.add(6);
    pvL.add(7);
    pvL.add(8);
    pvL.add(9);
    //  pvL.add(ss.posValuesL);
    //TODO  direct copy to get it right

    //  print('------- list pvl  b------------------');
    //  print(pvL);
    //  print(pvL.length);
    //  print('------- list pvl  b------------------\n');

    for (var c = 0; c < 9; c++) {
      //TODO  Think: about this
      if (matrix99[row][c] > 0) {
        int iAway = matrix99[row][c];
        //  pvL.remove(iAway);
        pvL.removeWhere((value) => value == iAway);
        //  print(iAway);
        //  String rmlS = matrix99[row][c].toString();
        //  print('MIR:Removing from pvL List: $rmlS ');

        //  print('------- list pvl a ------------------');
        //  print(pvL.length);
        //  print(pvL);
        //  print('------- list pvl  a------------------\n');

      }
    }
    //  print(pvL);
    mirL = pvL;
    //  print('------- list mirL a ------------------');
    //  print(mirL);
    //  print(mirL.length);
    //  print('------- list mirL a done ------------------ \n');
    //TODO  at least 2 times in list are 2 values ??

    return mirL;
  }

  ///  Same as previous but returning String
  String checkMissingInRowS(int row) {
    //  usage: 9
    StringBuffer sB = new StringBuffer();
    List l = checkMissingInRow(row);
    //  4 of 9 times this gives one value.. length(1)
    //  print('---- l MIR-S  return list ----------------------------');
    //  print(l);
    //  print(l.length);
    //  print('-------- l r l ------------------------');

    String s = l.toString();
    if (l.length == 0) sB.write('No errors.MIR-S ');

    if (l.length > 0) sB.write('Errors.-MIR-S:  $l ');
    //  print(l.length);

    sB.write(s);
    String sBs = sB.toString();
    return sBs;
  }

  ///  testing two previous functions
  void useMissingInRow() {
    print(' ROW MISSING CHECKING ');

    print(checkMissingInRowS(0));
    print(checkMissingInRowS(1));
    print(checkMissingInRowS(2));
    print(checkMissingInRowS(3));
    print(checkMissingInRowS(4));
    print(checkMissingInRowS(5));
    print(checkMissingInRowS(6));
    print(checkMissingInRowS(7));
    print(checkMissingInRowS(8));

    print(' ROW MISSING CHECKING ');
  }

  /// test row and return String
  String checkRowS(int r) {
    StringBuffer sB = new StringBuffer();
    StringBuffer mB = new StringBuffer(); // missing numbers

    int rs = 0;
    sB.write(' RowCheck:');
    //TODO  Not going right   List<int> pvL = ss.posValuesL; // 9 possible values
    List<int> pvL = [];

    ///TODO  funny  List<int>  testing
    pvL.add(1);
    pvL.add(2);
    pvL.add(3);
    pvL.add(4);
    pvL.add(5);
    pvL.add(6);
    pvL.add(7);
    pvL.add(8);
    pvL.add(9);

    for (var c = 0; c < 9; c++) {
      String sS = matrix99[r][c].toString();
      sB.write(' $sS');
      if (pvL.indexOf(matrix99[r][c]) < 0) {
        String missingNS = matrix99[r][c].toString();
        mB.write('Msg: $missingNS ');
      }
      rs = rs + matrix99[r][c];
    }
    String rS = rs.toString();
    int rsMisI = (ss.rowGoal - rs); //  ss.rowGoal = 45
    String rsMisS = rsMisI.toString();
    sB.write('  Sum: $rS $rsMisS  $mB');

    return sB.toString();
  } //     -----  checkRowS

//  checking matrix99 against answer sudoku. NOT USED!!
//  TODO  Add parameter to:  simulate random errors to table
  void checkMatrix() {
    //TODO  absolutely   add parameters
    //GLORY:
    print('\n ----------check Matrix --------------------');
    var s = matrix99;

    ///  use short-name var to avoid writing repeatedly longUglyFunctionNames
    var a = matrix99Answer;
    int eC = 0; //  error count
    //  bool b = false;
    List<String> errorL = [];
    for (var r = 0; r < 9; r++) {
      for (var c = 0; c < 9; c++) {
        if ((s[r][c] < a[r][c]) || s[r][c] > a[r][c]) {
          eC++;
          print(' ERROR-IN-MATRIX - ANSWER row: $r');
          print('M:  $matrix99[r]');
          print('A:  $matrix99Answer[r]');
          print('---------------------------------');

          int sI = s[r][c];
          String sS = sI.toString();
          int aI = a[r][c];
          String aS = aI.toString();
          String posS = " $r  $c ";
          String pS = ('S: $sS   A: $aS');
          errorL.add('error-CM::  $eC >  $pS   at: $posS');
        }
      }
    }
    print('---------- el ------------------------');
    print('ErrorCount:  $eC ');
    if (errorL.length > 0) errorL.forEach(print);
    print('---------- el done--------------------\n');
  } //     -----  checkMatrix

//  check update-value against answer table.
  void checkAgainstAnswer(int x, int y, int value, String actor) {
    String mRowS = matrix99[x].toString();
    String aRowS = matrix99Answer[x].toString();
    var z = matrix99Answer[x][y];
    if ((z > value) || (z < value)) {
      print('\n :CAA: CERROR not in Answer Table. Actor: $actor');
      print('R: $x  C: $y   Value: $value ');
      print('A-R:  $aRowS ');
      print('M-99: $mRowS ');
    }
  } //     -----  checkAgainstAnswer

  ///  display variable values and statistics at the end
  void reportChecker() {
    print('\n :END:info = = = = = = = =  end info = = = = = = = = ');

    String nodeHandlerCountS = cc.nodeHandlerCount.toString();
    String solvedHandlerCountS = cc.solvedHandlerCount.toString();

    String solvedAtEndS = cc.solvedAtEnd.toString();
    String solvedAtBegS = cc.solvedAtBeginning.toString();

    print('nodeHandlerCount  $nodeHandlerCountS');
    print('solvedHandlerCount  $solvedHandlerCountS');

    cc.solvedL.forEach(print);
    print(cc.solvedL.length);
    print('solvedAtBeg  $solvedAtBegS: solvedAtEnd     $solvedAtEndS: ');

    print('');
    print(':END:info  = = = = = = = =  end info done  = = == = = = =  \n');
  }
} //   -----------------  CheckerClass

///  try to use to this..hard to remember
var chk = new CheckerClass();

///  Data nodes of matrix99
var matrix99 = ([
  [0, 0, 0, 0, 0, 0, 6, 8, 0], //  Difficult
  [0, 0, 5, 7, 0, 0, 1, 0, 0], // row 1
  // testing.. little help added to sudoku
  //[0, 0, 6, 0, 3, 8, 0, 0, 0],
  [4, 0, 6, 2, 3, 0, 9, 0, 7], // row giving more help

  [5, 6, 0, 0, 8, 0, 0, 0, 0],
  [1, 0, 0, 0, 0, 0, 0, 0, 9],
//  [0, 0, 0, 0, 2, 0, 0, 7, 6], // Testing little help
  [8, 3, 0, 1, 0, 9, 0, 7, 6], // row giving more help

  [0, 0, 0, 3, 5, 0, 7, 0, 0],
  [0, 0, 2, 0, 0, 1, 4, 0, 0],
  [0, 9, 8, 0, 0, 0, 0, 0, 0],
]);

///  data nodes of matrixAnswer
var matrix99Answer = ([
  [2, 7, 3, 9, 1, 5, 6, 8, 4],
  [9, 8, 5, 7, 4, 6, 1, 3, 2],
  [4, 1, 6, 2, 3, 8, 9, 5, 7],

  [5, 6, 9, 4, 8, 7, 3, 2, 1],
  [1, 2, 7, 5, 6, 3, 8, 4, 9],
  [8, 3, 4, 1, 2, 9, 5, 7, 6],

  [6, 4, 1, 3, 5, 2, 7, 9, 8],
  [7, 5, 2, 8, 9, 1, 4, 6, 3],
  [3, 9, 8, 6, 7, 4, 2, 1, 5], //     row 8
]);

/// :LEARN  in Dart: void-function is #No-returning-value  function
///  :QUEST   build this with:  for (var x in matrix99 )  !!
void matrixShow(List<List<int>> _listM, String msg) {
  //  4 uses
  print('\n -- $msg by Heikki K.L.--');
  print(':Sudoku Difficult: With checkRowS ');
  for (var i = 0; i < 9; i++) {
    //TODO  modify output >> to use spaces after 3 nod
    //  so:  3,7,8  5,4,2, 1,6,9
    StringBuffer msB = new StringBuffer(); //  :notUsed: >
    String s1 = chk.checkRowS(i); // > this gives checkin data
    String s2 = matrix99[i].toString();
    print('$s1  $s2');
  }
  print('--------------------------- \n');
}

///  Function to return values-List of needed matrix99 (vertical)column
///  uses  6
List<int> matrixColumnFL(int colNum) {
  List<int> mcL = [];
  for (var x = 0; x < 9; x++) mcL.add(matrix99[x].elementAt(colNum));
  return mcL;
}

///  This works lol.  used: 1,  in ma-in() / mainRun
///  Checkable print-out of matrice columns. Using previous function
void matrixColumns() {
  print('\n -------------- matrixColumns  ------------');
  List<int> clsSumsL = [];
  List<int> clsErrorsL = [];

  int i = 0;
  for (var c = 0; c < 9; c++) {
    List<int> l = matrixColumnFL(c);
    for (var x = 0; x < 9; x++) i = i + (l[x]);
    clsSumsL.add(i);

    if (i < ss.rowGoal || i > ss.rowGoal) {
      //TODO do not exist    errorsInMc++;
    }
    clsErrorsL.add(i - ss.rowGoal);
    i = 0;
  }
  print(clsSumsL);
  print(clsErrorsL);
  print('-------------- matrixColumns  done ------- \n ');
}

///  function to check whether int is in vertical columns
///  should this return type be List ?  NO.
///  used: 1,  in blockSolver NOT: BlockSolver is now EMPTY :)
///  QUEST: Small one :   is this good practice?
int verticalCheck(int _fZero, int _sZero, int sNum) {
  //  --  :loc  30
  int _retCom = -1;

  List<int> _mcLFirst = (matrixColumnFL(_fZero));
  List<int> _mcLSecond = (matrixColumnFL(_sZero));

  int first = _mcLFirst.indexOf(sNum);
  int second = _mcLSecond.indexOf(sNum);

  bool firstContains = false;
  bool secondContains = false;

  if (first > -1) firstContains = true;

  if (second > -1) secondContains = true;

  if ((firstContains) && (!secondContains))
    _retCom = 2; //   hklTry => update second

  if ((secondContains) && (!firstContains)) _retCom = 1;
  if ((firstContains) && (secondContains)) _retCom = 3;
  //   hklTry  not :used: 3 => update neither
  return _retCom;
} //     -----  verticalCheck

///  in success calls matrix update node
///  handles ALL activities in single node
///  Think: Should ALL node-things be in one place??
void nodeHandler() {
  //  code:
}

/// Handling all-9-numbers in 9-rows and 9-columns   :loc  64
int rowHandler() {
  print(':ROUND:RH:');
  int _solvedInRowHandler = 0;
  int _doneCount = 0;
  int _doMax = 1;
  actor = 'rowHandler';
  for (var _nbr = 1; _nbr < 10; _nbr++) {
    for (var sRowX = 0; sRowX < 9; sRowX++) {
      for (var sColX = 0; sColX < 9; sColX++) {
        if (ss.valueXCanBeInNode(sRowX, sColX, _nbr)) {
          //  print('ROW-HANDLER IS WAKE');
          //DONE: limit this to 2 succes / round to give others a change
          if (_doneCount < _doMax) {
            if (ss.matrixCheckNode(sRowX, sColX, _nbr, actor)) {
              //  do statistics.. ?
              //  this calls: updateNode
              _doneCount++;
              _solvedInRowHandler++; // TODO  here??
            }
          }
          //TODO  use common updateNode
        } //  handle node   -----  node ==  0,  unsolve
      } //  handle columns,    >> all 9  nodes  in current row
    } //  handle all nine rows
  }
  actor = 'unknown-a-RH';
  //DO:     cc.solvedM.addAll({_srh[0]: _srh[1]});
  // .putIfAbsent(6, () => 5);
  print('  :ROUND:RH:d:');
  return _solvedInRowHandler;
} //     ----------  function  rowHandler

///  Handling 3 x 9 Block and calling update to find numbers
///  inside this is verticalCheck, that handles column-seeking
List<int> blockSolver(int _beginningRow) {
  actor = 'blockSolver';
  rr.blockSolverCount++; //  2 uses
  List<int> _retBSL = []; // 4 uses

  //  deleted all code.. build new
  //  and decide if use verticalCheck(

  return _retBSL; //   [round, solvedHere]
  //  print(':: BLOCK-SOLVER  ending ');
} //    --------------------     blockSolver

///  one use in solveSudoku
int blockHandler() {
  print(':ROUND:BH:');
  int solvedInBlockHandler = 0;
  //  print('runningNow: blockHandler::');
  //TODO  not using this yet
  List<int> loopBackL = []; // is only here... lol
  // This gives errors, and other code did solve sudoku. :lol
  loopBackL.addAll(blockSolver(0));

  ///TODO  do not use this, when all is still a mess
  ///WTF is this loopBackL anyway.  name: ?  idea: ?
  loopBackL.addAll(blockSolver(3));
  //  addAll({_srh[0]: _srh[1]});
  //      // .putIfAbsent(6, () => 5);
  cc.solvedInBlockSolverM.putIfAbsent(6, () => 5);
  loopBackL.addAll(blockSolver(6));
  print('  :ROUND:BH:d:');
  return solvedInBlockHandler;
} //     -----     blockHandler

///  Name "Spider" came, when in TV just happend to come SpiderMan :)
///  Name: always looking to-find original, easy-to-find names
///  go round and checks: 9 cubix areas: for 9 numbers  loc: 127
int spiderHandler() {
  print(':ROUND:SH:');
  actor = "SpiderHandler";
  int _spiderSolvedInSRound = 0; //  return-value
  int _doMaxCount = 3;
  int _doneCount = 0;

  void callSpider(int xRow, int xCol) {
    for (var value = 1; value < 10; value++) {
      //TODO  ugly.  using ALMOST loop actor inside loop
      int _value = value;
      rr.spiderCountRound++;
      for (var r = xRow; r < xRow + 3; r++) {
        for (var c = xCol; c < xCol + 3; c++) {
          if (matrix99[r][c] == 0) {
            rr.spiderCountTested++;

            //Temporary limit to give others a change to try
            if (_spiderSolvedInSRound < 3) {
              if (ss.matrixCheckNode(r, c, _value, actor)) {
                _spiderSolvedInSRound++;
                rr.spiderCountSolved++;
                //  print(':CS:Spider:solved: $r  $c  V: $_value');
              }
            }
            List<int> _guL = [];
            _guL.addAll(ss.valuesPossibleHereLF(r, c, _value));
            //  temporary limit
            if ((_guL.length == 2) && (_spiderSolvedInSRound < 3)) {
              String _guLS = _guL.toString();
              String _guLLengthS = _guL.length.toString();

              int valueOne, valueTwo;
              int _vOneC;
              int _vTwoC;
              //_guL[0}
              valueOne = _guL[0];
              //_guL[1]
              valueTwo = _guL[1];
              //  should start fom cubixBase 0,0

              //  in how many NODEs value can be in cubix
              //NOTE  we are working in BASE-node..

              _vOneC = cubixPossiblePlacesForValue(xRow, xCol, valueOne);
              _vTwoC = cubixPossiblePlacesForValue(xRow, xCol, valueTwo);
              // using:   bool valueXCanBeInNode(int r, int c, int v)
              String _v1S = valueOne.toString();
              String _v2S = valueTwo.toString();
              String _vOneCS = _vOneC.toString();
              String _vTwoCS = _vTwoC.toString();

              //print(':DBG:CS:V1/2:: $_v1S  $_v2S Places: $_vOneCS  $_vTwoCS');

              // if ONLY other of above is 1, update matrix...
              //  HklTry: deleted other && alternatives here
              if (_vOneC == 1) {
                print(':CS: GLORIOUS FIND should update First-to-node');
                ss.updateNode(r, c, valueOne, actor);
                print(':DBG:Spider :UPD:  one :NO-PRINT: ');
              } // update first to NODE

              //TODO  logic is not complete. Both can be 1 !!! NO!!
              //BUT NOT IN THUS NODE.. so which one num to update here?
              //  so update OTHER when OTHER have many places
              if (_vTwoC == 1) {
                print(':CS: GLORIOUS FIND should update Second-to-node');

                ss.updateNode(r, c, valueTwo, actor);

                print(':DBG:Spider :UPD:  two  :NO-PRINT: ');
              } //update second to NODE

              /*
                     if (ss.matrixCheckNode(r, c, _value, actor)) {
              _spiderSolvedInSRound++;
              rr.spiderCountSolved++;
              print(':SH: Spider :solved: $r  $c  $_value');
            }


               */
              //  ready to update node IF !!  int xRow, int xCol

            } //  ----- _guL length == 2

            //  if ONLY-TWO-VAL possible- DO-CROSS-CHECKING

            //  IT  MUST  ALL HAPPEN  HERE  NO  BIG  TWISTS
            //  *****************************************
          } //  ------   matrix.. = 0
        } //  ----------    col
      } //  ------------   row
    } //  ---------  value
  } //  --------------  call spider

  callSpider(0, 0);
  callSpider(0, 3);
  callSpider(0, 6);
  //  print('---------------------------  3 spider calls done ---');
  callSpider(3, 0);
  callSpider(3, 3);
  callSpider(3, 6);
  //  print('-----------------------------  6 spider calls done ---');
  callSpider(6, 0);
  callSpider(6, 3);
  callSpider(6, 6);

  //  print('-------------------   spider round over  -------- ---');
  actor = "unknown";
  //  print('spiderSolvedCount::   $spiderSolvedCount');
  print('  :ROUND:SH:d:');
  return _spiderSolvedInSRound;
} //  --------------  spiderHandler

///  Tests, that you run before main-run. TODO move to TestClass
void beforeRunTest() {
  print('********************  beforeRunTest *******************************');
  String s1 = chk.checkRowS(2);
  String s2 = chk.checkRowS(4);
  String s3 = chk.checkRowS(7);

  print('Row 1 sum before solving:     $s1');
  print('Row 4 sum before solving:     $s2');
  print('Row 7 sum before solving:     $s3');

  matrixShow(matrix99, 'Before Solving');

  tc.testBaseNodes();

  chk.useMissingInRow();

  print('********************  beforeRunTest done **************************');
}

///  Tests, that you run after main-run. TODO move to TestClass
void afterRunTest() {
  print('******************** afterRunTest *********************************');
  String s1 = chk.checkRowS(2);
  String s2 = chk.checkRowS(4);
  String s3 = chk.checkRowS(7);

  print('Row 1 sum After Solving:     $s1');
  print('Row 4 sum After Solving:     $s2');
  print('Row 7 sum After Solving:     $s3');

  matrixShow(matrix99, 'After Solving');

  tc.testBaseNodes();

  chk.useMissingInRow();
  print('******************** afterRunTest done *****************************');
}

///:LEARN   Every Dart programs execution begins at main method
void main() {
  print(':main: ..................   main  ...........................');
  var _timer = new Stopwatch()..start();

  //  beforeRunTest();
  //  matrixShow(matrix99, '99  Before Solving');
  //  matrixShow(matrix99Answer, 'Answer Before Solving');
  //  chk.beginStats(true); // random aso.
  //  tc.testProperties();
  //  tc.testCubix('Before Solving');

  solveSudoku();
  matrixShow(matrix99, '99 SolvedHereNow');

  void mainRun() {
    chk.reportChecker();
    ss.reportSudoku();
    rr.reportRunner();
    matrixShow(matrix99Answer, '99 Answer. After solve.');

    tc.testCubix('AfterSolving');
    cc.infoCounterMap();
    afterRunTest();
    matrixColumns();
    print('valueOnlyPossibleHereC:: $valueOnlyPossibleHereC');
    //  print(foundBuf);
    for (var r = 0; r < 9; r++) print(matrix99Answer[r]);
    print('\n  <= Answer -------------------- doneHere => \n');
    for (var r = 0; r < 9; r++) print(matrix99[r]);
    spiderInfo();
    cc.roundM.forEach((k, v) => print(' RoundM:  $k SolvedM:  $v'));
  }
  //  mainRun();

  String elapsed = _timer.elapsedMilliseconds.toString();
  print('Elapsed:  $elapsed   ms');

  print(':main: .................   main done ..............................');
} //  ------------------------   main

///  in how many times : value : can be set in cubix
///  Watch in every node : if value can be put there
int cubixPossiblePlacesForValue(int xRow, int yCol, int value) {
  int retInt = 0;
  for (var r = xRow; r < xRow + 3; r++) {
    for (var c = yCol; c < yCol + 3; c++) {
      if (matrix99[r][c] == 0) {
        //  called function returns List<int>
        //TODO  Oops
        //  DO-NOT-USE::  _l.addAll(ss.vbHereLF(r,c,value));
        if (ss.valueXCanBeInNode(r, c, value)) retInt++;
      } //  count values
    } // col
  } //  row
  return retInt;
}

///  function :: collecting other nodes based on #base#node location
///  homeNode parameter has two elements; x and y axis of current node
///:QUEST  can this be made better  :MESSY
///
List<int> cubixValuesLF(List<int> homeNode) {
  //  one use, by nice : cubixGetData
  //  move parameters to _x and _y (private) variables
  int _x = homeNode[0];
  int _y = homeNode[1];
  List<int> _cubeL = [];
  //  Move 3 to right and in every phase 3 to up to collect 3x3 matrix data
  //:FUNNY  :LEARN no mystic in these fo x... names
  for (var uglyDucly = _x; uglyDucly < _x + 3; uglyDucly++) {
    for (var snowflake = _y; snowflake < _y + 3; snowflake++) {
      //  now we must trust, that we are in #right node in matrix99
      // add > 0 data from matrix-cube to _cubeL List
      if (matrix99[uglyDucly].elementAt(snowflake) > 0) {
        _cubeL.add(matrix99[uglyDucly].elementAt(snowflake));
      }
    }
  }
  //  order of nodes in return list is not important, but smiling is :)
  return _cubeL;
}

///  Find all nodes in cubix, based on row - colon  location
///  parameters should be in  1 - 9 ( not checked )
///  parameters mark #currentNode
///:MESSY ?
List<int> cubixFindBaseLF(int xRow, int yCol) {
  List<int> retList = [];
  int _xRow, _yCol;
  _xRow = xRow;
  _yCol = yCol;

  /// base row must be  1,  4 or  7
  /// so. in row node [0]  [3]  or  [6]
  /// must be (math) method >> better solution.

  if (_xRow < 3) _xRow = 0;
  if ((_xRow > 2) && (_xRow < 6)) _xRow = 3;
  if (_xRow > 5) _xRow = 6;
  //  change yColon to "base" node value
  if (_yCol < 3) _yCol = 0;

  if ((_yCol > 2) && (_yCol < 6)) _yCol = 3;

  if (_yCol > 5) _yCol = 6;

  retList.add(_xRow);
  retList.add(_yCol);
  //  print(':CFB: R: $_xRow   C: $_yCol RL $retList    A: $actor');
  return retList; //  ao.  [rowInt, colInt]
}

///  Function  returns a List with numbers(>0)  from 3 x 3 matrix
///  called with parameters   cubixGetData(sRowX, sColX)
///  Parameters can be anywhere in 3 x 3 area. solveSudoku function uses this.
List cubixGetData(int x, int y) {
  // parameter _node should give _current_node's placement in 9x9 matrix
  int _x = x;
  int _y = y;
  //  Find Base of current #location.. / node
  //  change parameter to "base" values,,   so  1, 4, or 7
  //  calling other function here, that gives #base of this 3x3 cubix
  List<int> _baseL = cubixFindBaseLF(_x, _y);

  //  modify local variables to 1, 4, or 7
  //  just >> to see them, not using them
  _x = _baseL[0];
  _y = _baseL[1];

  ///  are you still with me ??   :)
  ///call  List<int> cubixValuesLF(List<int> homeNode //  5 uses
  /// calling other function here, with parameter _baseL List to get data
  List<int> collectorList = (cubixValuesLF(_baseL));
  return collectorList; //  like:  [4, 6, 7]
}

///  To make sudoku solver small / clean, move stuff here
void firstRoundStuff() {
  if (rr.firstRoundB) {
    print(' * * *  :ONLY-ONCE:  SS-First-round-stuff  * * * ');
    rr.firstRoundB = false;
  }
}

///  Be ready to move some stuff from sudoku solver to here
void lastRoundStuff() {
  //TODO  :Learn: howTo: Lambda function for if-clauses
  if (rr.lastRoundB) {
    //  grab code: from trash...
    print(' * * *  :ONLY-ONCE:  SSLast-round-stuff  :TEST: * * *');
  }
}

///  handle and show terminated-state variables
void terminatedStuff() {
  //  to keep if-structures from polluting sudokuSolver-function
  print(' ');
  if (rr.roundCount == rr.roundMax) {
    print('T T T T T   TERMINATED:  maxCount reached  T T T T T T ');
    print('rr roundCount and roundMax:: ');
    print(rr.roundCount);
    print(rr.roundMax);
    print('T T T T T T T T T T T T T T T T T T T T T T T T T T T\n');
    ss.failedB = true; //  ss.failedB boolean marks end of loop
    rr.lastRoundB = true; //  to get more print info in last round
  }
}

///  Logic of actual flow of solving
///TODO  Check bool and logic variables activity
void solveSudoku() {
  actor = 'solveSudoku';
  ss.failedB = false; // used: 5
  ss.solvedB = false; //  3

  rr.firstRoundB = true; // 2
  rr.lastRoundB = false; //  4

  rr.failedC = 0; //  7
  rr.failedRoundLimit = 1;
  rr.successC = 0; //  21 / 19

  //  lol
  rr.blockSolverCount = 0; //  4

  do {
    rr.roundCount++;
    rr.successC = 0; // new
    firstRoundStuff();

    if (rr.failedC > (rr.failedRoundLimit)) {
      print(':FINAL-DOOM:  NEWER HAPPENDS: 1 ');
      ss.failedB = true;
      rr.lastRoundB = true; //  oneMore hack
    }
    terminatedStuff();
    //TODO  add notSolvedAllNodes right  check

    lastRoundStuff();
    rr.successC = rr.successC + rowHandler();

    rr.successC = rr.successC + blockHandler();
    rr.successC = rr.successC + spiderHandler();
    rr.spiderCountCalled++;
    rr.successC = rr.successC + rr.successC;
    rr.info();

    if (rr.successC == 0) rr.failedC++;

    if (rr.successC == 0) print(' * * *  :Lambda-Work??: NO!!');
    if (rr.successTotalC == ss.sudokuGoal) ss.solvedB = true;
    if (rr.failedC > rr.failedRoundLimit) ss.failedB = true;

    cc.roundM.putIfAbsent(rr.roundCount, () => rr.successC);
  } while ((!ss.solvedB) && (!ss.failedB));

  String rrEndS = rr.infoEnd();
  print(':SS:End: SolvedInSudokuSolvers three methods:: $rrEndS \n');
}
